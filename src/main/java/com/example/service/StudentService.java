package com.example.service;

import java.util.Date;
import java.util.List;

import com.example.model.StudentDetailModel;
import com.example.model.StudentModel;

public interface StudentService {
	
	StudentModel selectPeserta(String nomor);
	List<StudentModel> selectStudentFromProdi (String kodeProdi);
	List<StudentDetailModel> selectStudentFromUniv (String kodeUniv);
	int getAge(Date tglLahir);
	String dateToString(Date tgl);
	StudentModel getOlderStudent(List<StudentModel> students);
	StudentModel getYoungerStudent(List<StudentModel> students);

}
