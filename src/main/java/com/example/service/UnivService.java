package com.example.service;

import java.util.List;

import com.example.model.UnivModel;

public interface UnivService {
	
	UnivModel selectUniv(String kodeUniv);
	List<UnivModel> selectAllUniv ();
	void addUniv (UnivModel univ);
	void updateUniv (UnivModel univ);
	void deleteUniv (String kodeUniv);

}
