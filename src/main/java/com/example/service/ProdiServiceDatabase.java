package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.ProdiMapper;
import com.example.model.ProdiModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProdiServiceDatabase implements ProdiService {
	
	@Autowired
    private ProdiMapper prodiMapper;

	@Override
	public ProdiModel selectProdi(String kodeProdi) {
		// TODO Auto-generated method stub
		log.info ("select prodi with kode_prodi {}", kodeProdi);
		return prodiMapper.selectProdi (kodeProdi);
	}

	@Override
	public List<ProdiModel> selectAllProdi() {
		// TODO Auto-generated method stub
		log.info ("select all prodi");
        return prodiMapper.selectAllProdi ();
	}

	@Override
	public List<ProdiModel> selectProdiFromUniv(String kodeUniv) {
		// TODO Auto-generated method stub
		log.info ("select all prodi with kode_univ {}", kodeUniv);
        return prodiMapper.selectProdiFromUniv (kodeUniv);
	}

	@Override
	public void addProdi(ProdiModel prodi) {
		// TODO Auto-generated method stub
		prodiMapper.addProdi (prodi);
	}

	@Override
	public void updateProdi(ProdiModel prodi) {
		// TODO Auto-generated method stub
		log.info("prodi " + prodi.getKodeProdi() + " updated");
		prodiMapper.updateProdi(prodi);
	}

	@Override
	public void deleteProdi(String kodeProdi) {
		// TODO Auto-generated method stub
		log.info("prodi " + kodeProdi + " deleted");
		prodiMapper.deleteProdi (kodeProdi);
	}

}
