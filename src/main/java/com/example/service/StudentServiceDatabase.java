package com.example.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.StudentMapper;
import com.example.model.StudentDetailModel;
import com.example.model.StudentModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class StudentServiceDatabase implements StudentService {
	
	@Autowired
    private StudentMapper studentMapper;

	@Override
	public StudentModel selectPeserta(String nomor) {
		// TODO Auto-generated method stub
		log.info ("select peserta with nomor {}", nomor);
		return studentMapper.selectPeserta (nomor);
	}
	
	@Override
	public List<StudentModel> selectStudentFromProdi(String kodeProdi) {
		// TODO Auto-generated method stub
		log.info ("select all student with kode_prodi {}", kodeProdi);
        return studentMapper.selectStudentFromProdi (kodeProdi);
	}
	
	@Override
	public List<StudentDetailModel> selectStudentFromUniv(String kodeUniv) {
		// TODO Auto-generated method stub
		log.info ("select all student with kode_univ {}", kodeUniv);
        return studentMapper.selectStudentFromUniv (kodeUniv);
	}

	@Override
	public int getAge(Date tglLahir) {
		// TODO Auto-generated method stub
		Calendar today = Calendar.getInstance();
	    Calendar birthDate = Calendar.getInstance();

	    int age = 0;

	    birthDate.setTime(tglLahir);

	    age = today.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR);

	    // If birth date is greater than todays date (after 2 days adjustment of
	    // leap year) then decrement age one year
	    if ((birthDate.get(Calendar.DAY_OF_YEAR)
	            - today.get(Calendar.DAY_OF_YEAR) > 3)
	            || (birthDate.get(Calendar.MONTH) > today.get(Calendar.MONTH))) {
	        age--;

	        // If birth date and todays date are of same month and birth day of
	        // month is greater than todays day of month then decrement age
	    } else if ((birthDate.get(Calendar.MONTH) == today.get(Calendar.MONTH))
	            && (birthDate.get(Calendar.DAY_OF_MONTH) > today
	                    .get(Calendar.DAY_OF_MONTH))) {
	        age--;
	    }
	    return age;
	}

	@Override
	public String dateToString(Date tgl) {
		// TODO Auto-generated method stub
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String strDate = df.format(tgl);
		return strDate;
	}

	@Override
	public StudentModel getOlderStudent(List<StudentModel> students) {
		// TODO Auto-generated method stub
		StudentModel varStudent = students.get(0);
		for (int i = 1; i < students.size(); i++) {
			if (students.get(i).getTglLahir().before(varStudent.getTglLahir())){
				varStudent = students.get(i);
			}
		}
		return varStudent;
	}

	@Override
	public StudentModel getYoungerStudent(List<StudentModel> students) {
		// TODO Auto-generated method stub
		StudentModel varStudent = students.get(0);
		for (int i = 1; i < students.size(); i++) {
			if (students.get(i).getTglLahir().after(varStudent.getTglLahir())){
				varStudent = students.get(i);
			}
		}
		return varStudent;
	}

}
