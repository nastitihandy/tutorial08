package com.example.service;

import java.util.List;

import com.example.model.ProdiModel;

public interface ProdiService {
	
	ProdiModel selectProdi(String kodeProdi);
	List<ProdiModel> selectAllProdi ();
	List<ProdiModel> selectProdiFromUniv (String kodeUniv);
	void addProdi (ProdiModel prodi);
	void updateProdi (ProdiModel prodi);
	void deleteProdi (String kodeProdi);

}
