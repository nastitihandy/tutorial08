package com.example.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
@NoArgsConstructor
public class StudentDetailModel extends StudentModel {
	
	public String namaProdi;

}
