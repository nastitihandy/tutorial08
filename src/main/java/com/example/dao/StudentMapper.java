package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.example.model.StudentDetailModel;
import com.example.model.StudentModel;

@Mapper
public interface StudentMapper {
	
	@Select("select nomor,nama,tgl_lahir,kode_prodi "
    		+ "from peserta where nomor = #{nomor} ")
    @Results(value = {
    		@Result(property="nomor", column="nomor"),
    		@Result(property="nama", column="nama"),
        	@Result(property="tglLahir", column="tgl_lahir"),
        	@Result(property="kodeProdi", column="kode_prodi")
        })
	StudentModel selectPeserta (@Param("nomor") String nomor);
	
	@Select("select nomor,nama,tgl_lahir,kode_prodi "
    		+ "from peserta where kode_prodi = #{kodeProdi} ")
    @Results(value = {
    		@Result(property="nomor", column="nomor"),
    		@Result(property="nama", column="nama"),
        	@Result(property="tglLahir", column="tgl_lahir"),
        	@Result(property="kodeProdi", column="kode_prodi")
        })
	List<StudentModel> selectStudentFromProdi (@Param("kodeProdi") String kodeProdi);
	
	@Select("select a.*, b.nama_prodi from peserta a "
			+ "left outer join prodi b on b.kode_prodi = a.kode_prodi "
			+ "left outer join univ c on c.kode_univ = b.kode_univ "
			+ "where b.kode_univ = #{kodeUniv} ")
    @Results(value = {
    		@Result(property="nomor", column="nomor"),
    		@Result(property="nama", column="nama"),
        	@Result(property="tglLahir", column="tgl_lahir"),
        	@Result(property="kodeProdi", column="kode_prodi"),
        	@Result(property="namaProdi", column="nama_prodi")
        })
	List<StudentDetailModel> selectStudentFromUniv (@Param("kodeUniv") String kodeUniv);

}
