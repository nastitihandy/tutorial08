package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.example.model.ProdiModel;

@Mapper
public interface ProdiMapper {
	
	@Select("select kode_univ,kode_prodi,nama_prodi "
    		+ "from prodi where kode_prodi = #{kodeProdi} ")
    @Results(value = {
    		@Result(property="kodeUniv", column="kode_univ"),
    		@Result(property="kodeProdi", column="kode_prodi"),
        	@Result(property="namaProdi", column="nama_prodi")
        })
	ProdiModel selectProdi (@Param("kodeProdi") String kodeProdi);
	
	@Select("select kode_univ,kode_prodi,nama_prodi "
    		+ "from prodi ")
    @Results(value = {
    		@Result(property="kodeUniv", column="kode_univ"),
    		@Result(property="kodeProdi", column="kode_prodi"),
        	@Result(property="namaProdi", column="nama_prodi")
        })
	List<ProdiModel> selectAllProdi ();
	
	@Select("select kode_univ,kode_prodi,nama_prodi "
    		+ "from prodi where kode_univ = #{kodeUniv} ")
    @Results(value = {
    		@Result(property="kodeUniv", column="kode_univ"),
    		@Result(property="kodeProdi", column="kode_prodi"),
        	@Result(property="namaProdi", column="nama_prodi")
        })
	List<ProdiModel> selectProdiFromUniv (@Param("kodeUniv") String kodeUniv);
	
	@Insert("INSERT INTO prodi (kode_univ, kode_prodi, nama_prodi) "
    		+ "VALUES (#{kodeUniv}, #{kodeProdi}, #{namaProdi})")
    void addProdi (ProdiModel prodi);
	
	@Update("UPDATE prodi SET nama_prodi = #{namaProdi} "
    		+ "WHERE kode_prodi = #{kodeProdi}")
    void updateProdi (ProdiModel prodi);
	
	@Delete("DELETE FROM prodi WHERE kode_prodi = #{kodeProdi}")
    void deleteProdi (@Param("kodeProdi") String kodeProdi);

}
