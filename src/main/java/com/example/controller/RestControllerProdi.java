package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.ProdiModel;
import com.example.model.StudentModel;
import com.example.service.ProdiService;
import com.example.service.StudentService;
import com.example.service.UnivService;

@RestController
public class RestControllerProdi {
	
	@Autowired
	ProdiService prodiDAO;
	
	@Autowired
	StudentService studentDAO;
	
	@Autowired
	UnivService univDAO;
	
	@RequestMapping(value = "/rest/prodi", method = RequestMethod.GET)
	 public ProdiModel prodi(Model model, @RequestParam(value =
	"kode", required = false) String kodeProdi)
	 {
		ProdiModel prodi = prodiDAO.selectProdi (kodeProdi);
		
		if (prodi != null) {
        	List<StudentModel> students = studentDAO.selectStudentFromProdi (kodeProdi);
        	model.addAttribute ("prodi", prodi);
        	model.addAttribute ("students", students);
        	model.addAttribute ("univ", univDAO.selectUniv (prodi.getKodeUniv()));
        	if (students.size() != 0) {
        		model.addAttribute ("stOlder", studentDAO.getOlderStudent(students));
                model.addAttribute ("stYounger", studentDAO.getYoungerStudent(students));
                model.addAttribute ("umurOlder", studentDAO.getAge(studentDAO.getOlderStudent(students).getTglLahir()));
                model.addAttribute ("umurYounger", studentDAO.getAge(studentDAO.getYoungerStudent(students).getTglLahir()));
        	} else {
	            model.addAttribute ("stOlder", new StudentModel());
	            model.addAttribute ("stYounger", new StudentModel());
	            model.addAttribute ("umurOlder", new StudentModel());
	            model.addAttribute ("umurYounger", new StudentModel());
        	}
            return prodi;
        } else {
            model.addAttribute ("kodeProdi", kodeProdi);
            return prodi;
        }
	 }

}
