package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.model.ProdiModel;
import com.example.model.StudentDetailModel;
import com.example.model.StudentModel;
import com.example.model.UnivModel;
import com.example.service.ProdiService;
import com.example.service.StudentService;
import com.example.service.UnivService;

@Controller
public class ProdiController {
	
	@Autowired
	ProdiService prodiDAO;
	
	@Autowired
	StudentService studentDAO;
	
	@Autowired
	UnivService univDAO;
	
	@RequestMapping("/prodi")
	public String view (@RequestParam(value = "kode", required = false, defaultValue = "") String kodeProdi, Model model)
	{
		ProdiModel prodi = prodiDAO.selectProdi (kodeProdi);
		
		if (prodi != null) {
        	List<StudentModel> students = studentDAO.selectStudentFromProdi (kodeProdi);
        	model.addAttribute ("prodi", prodi);
        	model.addAttribute ("students", students);
        	model.addAttribute ("univ", univDAO.selectUniv (prodi.getKodeUniv()));
        	if (students.size() != 0) {
        		model.addAttribute ("stOlder", studentDAO.getOlderStudent(students));
                model.addAttribute ("stYounger", studentDAO.getYoungerStudent(students));
                model.addAttribute ("umurOlder", studentDAO.getAge(studentDAO.getOlderStudent(students).getTglLahir()));
                model.addAttribute ("umurYounger", studentDAO.getAge(studentDAO.getYoungerStudent(students).getTglLahir()));
        	} else {
	            model.addAttribute ("stOlder", new StudentModel());
	            model.addAttribute ("stYounger", new StudentModel());
	            model.addAttribute ("umurOlder", new StudentModel());
	            model.addAttribute ("umurYounger", new StudentModel());
        	}
            return "prodi-detail";
        } else {
            model.addAttribute ("kodeProdi", kodeProdi);
            return "prodi-not-found";
        }
	}
	
	@RequestMapping("/prodi/add")
    public String add (@RequestParam(value = "kodeUniv", required = false, defaultValue = "") String kodeUniv, Model model)
    {
		model.addAttribute ("kodeUniv", kodeUniv);
		model.addAttribute ("prodi", new ProdiModel());
        return "form-prodi-add";
    }


    @RequestMapping(value = "/prodi/add/submit", method = {RequestMethod.POST, RequestMethod.GET})
    public String addSubmit (Model model, @ModelAttribute ProdiModel prodi,
    		@RequestParam(value = "kodeUniv", required = false) String kodeUniv)
    {
    	UnivModel univ = univDAO.selectUniv (kodeUniv);
    	prodiDAO.addProdi (prodi);
    	List<ProdiModel> prodis = prodiDAO.selectProdiFromUniv (univ.getKodeUniv());
    	List<StudentDetailModel> students= studentDAO.selectStudentFromUniv(univ.getKodeUniv());
        model.addAttribute ("univ", univ);
        model.addAttribute ("prodis", prodis);
        model.addAttribute ("students", students);
        return "redirect:/univ/" + univ.getKodeUniv();
    }
    
    @RequestMapping("/prodi/update/{kodeProdi}")
    public String update (Model model, @PathVariable(value = "kodeProdi") String kodeProdi)
    {
    	ProdiModel prodi = prodiDAO.selectProdi (kodeProdi);

        if (prodi != null) {
        	model.addAttribute ("prodi", prodi);
            return "form-prodi-update";
        } else {
            model.addAttribute ("kodeProdi", kodeProdi);
            return "prodi-not-found";
        }
    	
    }
	
	@RequestMapping(value = "/prodi/update/submit", method = RequestMethod.POST)
    public String updateSubmit (@ModelAttribute ProdiModel prodi, Model model)
    {
		UnivModel univ = univDAO.selectUniv (prodi.getKodeUniv());
		prodiDAO.updateProdi(prodi);
		List<ProdiModel> prodis = prodiDAO.selectProdiFromUniv (univ.getKodeUniv());
    	List<StudentDetailModel> students= studentDAO.selectStudentFromUniv(univ.getKodeUniv());
        model.addAttribute ("univ", univ);
        model.addAttribute ("prodis", prodis);
        model.addAttribute ("students", students);
        return "redirect:/univ/" + univ.getKodeUniv();
    }
	
	@RequestMapping("/prodi/delete/{kodeProdi}")
    public String delete (Model model, @PathVariable(value = "kodeProdi") String kodeProdi)
    {
		ProdiModel prodi = prodiDAO.selectProdi (kodeProdi);
		
		if (prodi != null) {
			prodiDAO.deleteProdi(kodeProdi);
			List<ProdiModel> prodis = prodiDAO.selectProdiFromUniv (prodi.getKodeUniv());
	    	List<StudentDetailModel> students= studentDAO.selectStudentFromUniv(prodi.getKodeUniv());
	        model.addAttribute ("univ", univDAO.selectUniv (prodi.getKodeUniv()));
	        model.addAttribute ("prodis", prodis);
	        model.addAttribute ("students", students);
	        return "redirect:/univ/" + prodi.getKodeUniv();
        } else {
            model.addAttribute ("kodeProdi", kodeProdi);
            return "prodi-not-found";
        }
		
    }

}
