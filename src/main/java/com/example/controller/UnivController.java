package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.model.ProdiModel;
import com.example.model.StudentDetailModel;
import com.example.model.UnivModel;
import com.example.service.ProdiService;
import com.example.service.StudentService;
import com.example.service.UnivService;

@Controller
public class UnivController {
	
	@Autowired
	UnivService univDAO;
	
	@Autowired
	ProdiService prodiDAO;
	
	@Autowired
	StudentService studentDAO;
	
	@RequestMapping("/univ")
    public String view (Model model)
    {
        List<UnivModel> univs = univDAO.selectAllUniv ();
        model.addAttribute ("univs", univs);

        return "univ-viewall";
    }
	
	@RequestMapping("/univ/{kodeUniv}")
    public String viewPath (Model model,
            @PathVariable(value = "kodeUniv") String kodeUniv)
    {
        UnivModel univ = univDAO.selectUniv (kodeUniv);

        if (univ != null) {
        	List<ProdiModel> prodis = prodiDAO.selectProdiFromUniv (kodeUniv);
        	List<StudentDetailModel> students= studentDAO.selectStudentFromUniv(kodeUniv);
            model.addAttribute ("univ", univ);
            model.addAttribute ("prodis", prodis);
            model.addAttribute ("students", students);
            return "univ-detail";
        } else {
            model.addAttribute ("kodeUniv", kodeUniv);
            return "univ-not-found";
        }
    }
	
	@RequestMapping("/univ/add")
    public String add (Model model)
    {
		model.addAttribute ("univ", new UnivModel());
        return "form-univ-add";
    }


    @RequestMapping(value = "/univ/add/submit", method = RequestMethod.POST)
    public String addSubmit (Model model, @ModelAttribute UnivModel univ)
    {
    	univDAO.addUniv (univ);
    	List<UnivModel> univs = univDAO.selectAllUniv ();
    	model.addAttribute ("univs", univs);
        return "redirect:/univ";
    }
    
    @RequestMapping("/univ/update/{kodeUniv}")
    public String update (Model model, @PathVariable(value = "kodeUniv") String kodeUniv)
    {
		UnivModel univ = univDAO.selectUniv (kodeUniv);

        if (univ != null) {
        	model.addAttribute ("univ", univ);
            return "form-univ-update";
        } else {
            model.addAttribute ("kodeUniv", kodeUniv);
            return "univ-not-found";
        }
    	
    }
	
	@RequestMapping(value = "/univ/update/submit", method = RequestMethod.POST)
    public String updateSubmit (@ModelAttribute UnivModel univ, Model model)
    {
		univDAO.updateUniv(univ);
		List<UnivModel> univs = univDAO.selectAllUniv ();
    	model.addAttribute ("univs", univs);
        return "redirect:/univ";
    }
	
	@RequestMapping("/univ/delete/{kodeUniv}")
    public String delete (Model model, @PathVariable(value = "kodeUniv") String kodeUniv)
    {
		UnivModel univ = univDAO.selectUniv (kodeUniv);
		
		if (univ != null) {
			univDAO.deleteUniv(kodeUniv);
			List<UnivModel> univs = univDAO.selectAllUniv ();
	        model.addAttribute ("univs", univs);
	        return "redirect:/univ";
        } else {
            model.addAttribute ("kodeUniv", kodeUniv);
            return "univ-not-found";
        }
		
    }

}
