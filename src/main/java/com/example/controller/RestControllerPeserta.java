package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.StudentModel;
import com.example.service.StudentService;

@RestController
public class RestControllerPeserta {
	
	@Autowired
	StudentService studentDAO;
	
	@RequestMapping(value = "/rest/peserta", method = RequestMethod.GET)
	 public StudentModel peserta(Model model, @RequestParam(value =
	"nomor", required = false) String nomor)
	 {
		// Implementasi anda yang lama
		StudentModel student = studentDAO.selectPeserta (nomor);
		
		if (student != null) {
        	model.addAttribute ("student", student);
        	model.addAttribute ("tglLahir", studentDAO.dateToString(student.getTglLahir()));
        	model.addAttribute ("umur", studentDAO.getAge(student.getTglLahir()));
            return student;
        } else {
            model.addAttribute ("nomor", nomor);
            return student;
        }
	 }

}
