package com.example.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.model.ProdiModel;
import com.example.model.StudentModel;
import com.example.model.UnivModel;
import com.example.service.ProdiService;
import com.example.service.StudentService;
import com.example.service.UnivService;

@Controller
public class SbmptnController {
	
	@Autowired
    StudentService studentDAO;
	
	@Autowired
	ProdiService prodiDAO;
	
	@Autowired
	UnivService univDAO;
	
	@RequestMapping(value = "404Error", method = RequestMethod.GET)
	public String throwRuntimeException() {
		return "error";
	}
	
	@RequestMapping("/")
    public String index ()
    {
        return "index";
    }
	
	@RequestMapping("/sbmptn")
    public String sbmptn ()
    {
        return "index";
    }
	
	@RequestMapping(value = "/pengumuman/submit", method = RequestMethod.POST)
    public String sbmptnSubmit (Model model, HttpServletRequest request)
    {
		String noPeserta = request.getParameter("inputNomor");
		StudentModel peserta = studentDAO.selectPeserta (noPeserta);
		
		if (peserta != null) {
            model.addAttribute ("peserta", peserta);
            model.addAttribute ("tglLahir", studentDAO.dateToString(peserta.getTglLahir()));
            model.addAttribute ("umur", studentDAO.getAge(peserta.getTglLahir()));
            
            if (peserta.getKodeProdi() != null){
            	ProdiModel prodi = prodiDAO.selectProdi (peserta.getKodeProdi());
            	UnivModel univ = univDAO.selectUniv (prodi.getKodeUniv());
            	model.addAttribute ("prodi", prodi);
            	model.addAttribute ("univ", univ);
            }
            
            return "pengumuman";
        } else {
            model.addAttribute ("noPeserta", noPeserta);
            return "peserta-not-found";
        }
    }

}
