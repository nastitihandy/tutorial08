package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.model.StudentModel;
import com.example.service.StudentService;

@Controller
public class StudentController {
	
	@Autowired
	StudentService studentDAO;
	
	@RequestMapping("/peserta")
	public String view (@RequestParam(value = "nomor", required = false, defaultValue = "") String nomor, Model model)
	{
		StudentModel student = studentDAO.selectPeserta (nomor);
		
		if (student != null) {
        	model.addAttribute ("student", student);
        	model.addAttribute ("tglLahir", studentDAO.dateToString(student.getTglLahir()));
        	model.addAttribute ("umur", studentDAO.getAge(student.getTglLahir()));
            return "student-detail";
        } else {
            model.addAttribute ("nomor", nomor);
            return "student-not-found";
        }
	}

}
